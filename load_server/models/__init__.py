# coding: utf-8

# flake8: noqa
from __future__ import absolute_import
# import models into model package
from load_server.models.machine_id import MachineId
from load_server.models.machine_info import MachineInfo
