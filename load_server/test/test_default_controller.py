# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from load_server.models.machine_id import MachineId  # noqa: E501
from load_server.models.machine_info import MachineInfo  # noqa: E501
from load_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_attach_instance(self):
        """Test case for attach_instance


        """
        machineId = MachineId()
        response = self.client.open(
            '/elb/{elbName}'.format(elbName='elbName_example'),
            method='POST',
            data=json.dumps(machineId),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_elb_elb_name_delete(self):
        """Test case for elb_elb_name_delete


        """
        machineId = MachineId()
        response = self.client.open(
            '/elb/{elbName}'.format(elbName='elbName_example'),
            method='DELETE',
            data=json.dumps(machineId),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_healthcheck_get(self):
        """Test case for healthcheck_get


        """
        response = self.client.open(
            '/healthcheck',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_machines_elb(self):
        """Test case for list_machines_elb


        """
        response = self.client.open(
            '/elb/{elbName}'.format(elbName='elbName_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
